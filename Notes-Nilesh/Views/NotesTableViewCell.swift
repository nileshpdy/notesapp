//
//  NotesTableViewCell.swift
//  Notes-Nilesh
//
//  Created by Nilesh Pandey on 03/04/18.
//  Copyright © 2018 Nilesh Pandey. All rights reserved.
//

import UIKit

class NotesTableViewCell: UITableViewCell {
    
    // MARK: - Variables and Constants
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    // MARK: - View life cycle events

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
