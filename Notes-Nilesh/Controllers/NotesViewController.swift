//
//  NotesViewController.swift
//  Notes-Nilesh
//
//  Created by Nilesh Pandey on 03/04/18.
//  Copyright © 2018 Nilesh Pandey. All rights reserved.
//

import UIKit

class NotesViewController: UITableViewController {

    // MARK: - Variables and Constants
    
    var notes = [Note]() {
        didSet {
            tableView.reloadData()
        }
    }

    // MARK: - View life cycle events
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notes = CoreDataHelper.retriveNotes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View Delegates and Data Source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotesTableViewCell", for: indexPath) as! NotesTableViewCell
        let note = notes[indexPath.row]
        cell.titleLabel.text = note.title
        cell.dateLabel.text = note.modifiedTime?.convertToString() ?? ""
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let note = notes[indexPath.row]
            CoreDataHelper.deleteNote(note: note)
            notes = CoreDataHelper.retriveNotes()
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "DisplayNote":
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            
            let note = notes[indexPath.row]
            let vc = segue.destination as! NoteDetailsViewController
            vc.note = note
            
        case "AddNote":
            print("create note bar button item tapped")
            
        default:
            print("unexpected segue identifier")
        }

    }
    
    // MARK: - Unwind Segue
    
    @IBAction func unwindWithSegue(_ segue: UIStoryboardSegue) {
        notes = CoreDataHelper.retriveNotes()
    }
    
}

// MARK: - extension Date

extension Date {
    func convertToString() -> String {
        return DateFormatter.localizedString(from: self, dateStyle: DateFormatter.Style.full, timeStyle: DateFormatter.Style.short)
    }
}



