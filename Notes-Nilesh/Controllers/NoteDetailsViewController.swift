//
//  NoteDetailsViewController.swift
//  Notes-Nilesh
//
//  Created by Nilesh Pandey on 03/04/18.
//  Copyright © 2018 Nilesh Pandey. All rights reserved.
//

import UIKit

class NoteDetailsViewController: UIViewController {

    // MARK: - Variables and Constants
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var note: Note?
    
    // MARK: - View life cycle events

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let note = note {
            titleTextField.text = note.title
            descriptionTextView.text = note.descriptionText
        } else {
            titleTextField.text = ""
            descriptionTextView.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "Save" where note != nil:
            note?.title = titleTextField.text ?? ""
            note?.descriptionText = descriptionTextView.text ?? ""
            note?.modifiedTime = Date()
            CoreDataHelper.saveNote()
            
        case "Save" where note == nil:
            let note = CoreDataHelper.newNote()
            note.title = titleTextField.text ?? ""
            note.descriptionText = descriptionTextView.text ?? ""
            note.modifiedTime = Date()
            CoreDataHelper.saveNote()
            
        case "Cancel":
            print("cancel bar button item tapped")
            
        default:
            print("unexpected segue identifier")
        }
    }

}

// MARK: - extension TextView

extension NoteDetailsViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if range.location == 0 && text == " " {
            return false
        }
        
        if textView.text.count < 300 || text == "" {
            return true
        } else {
            return false
        }
    }
    
}
